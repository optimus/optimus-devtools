export default class DevToolsClientBulmaSwitch
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/bulma_switch.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				loader(this.target, false)
			})
	}
}