
export default class DevToolsClientComposantsDraglist
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-devtools/client-doc/composants_map_demo.html', main)
		await load('/components/optimus_map.js', main,
			{
				GoogleMaps: true,
				GoogleStreetView: true,
				GeoSearch: true,
				Pointer: true,
				StartPoint: 'user',
			})
	}
}