export default class DevToolsClientBulmaCode
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/client-doc/bulma_code.html', this.target)
	}
}