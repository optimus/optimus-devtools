load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
import { Tabulator, EditModule, FormatModule, InteractionModule, KeybindingsModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([EditModule, FormatModule, InteractionModule, KeybindingsModule])
load_script('/libs/luxon.min.js')

export default class DevToolsClientComposantsPresentation
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/composants_datepicker.html', this.target)
			.then(() => 
			{
				highlight_code(main)

				document.querySelector('.date-picker').onclick = event => load('/components/optimus_datepicker.js', modal, { input: event.target.parentNode.parentNode.querySelector('input') })
				document.querySelector('.date-time-picker').onclick = event => load('/components/optimus_datepicker.js', modal, { input: event.target.parentNode.parentNode.querySelector('input') })
				document.querySelector('.time-picker').onclick = event => load('/components/optimus_datepicker.js', modal, { input: event.target.parentNode.parentNode.querySelector('input') })
				document.querySelector('.modal-button').onclick = () => modal.open('/services/optimus-devtools/client-doc/composants_datepicker_modal.js', true)

				let datepicker2 = load('/components/optimus_table/datepicker.js')
				this.tabulator = new Tabulator(main.querySelector('.optimus-table-container'),
					{
						layout: 'fitColumns',
						columns:
							[
								{
									field: 'id',
									title: 'ID',
									editor: 'input',
									minWidth: 100,
									maxWidth: 100,
								},
								{
									field: 'date',
									title: 'Date',
									hozAlign: 'center',
									minWidth: 120,
									maxWidth: 120,
									formatter: 'datetime',
									formatterParams:
									{
										inputFormat: "yyyy-MM-dd",
										outputFormat: "dd/MM/yyyy",
									},
									editor: datepicker2.open,
									editorParams:
									{
										optimusDatepicker:
										{
											type: 'date',
											allowKeyboardInput: store.touchscreen ? false : true,
											required: false
										}
									},
								},
								{
									field: 'datetime',
									title: 'DateTime',
									hozAlign: 'center',
									minWidth: 200,
									maxWidth: 200,
									formatter: 'datetime',
									formatterParams:
									{
										inputFormat: "yyyy-MM-dd HH:mm",
										outputFormat: "dd/MM/yyyy HH:mm",
									},
									editor: datepicker2.open,
									editorParams:
									{
										optimusDatepicker:
										{
											type: 'datetime',
											allowKeyboardInput: store.touchscreen ? false : true,
											required: false
										}
									},
								},
								{
									field: 'time',
									title: 'Time',
									hozAlign: 'center',
									minWidth: 120,
									maxWidth: 120,
									formatter: 'datetime',
									formatterParams:
									{
										inputFormat: "HH:mm",
										outputFormat: "HH:mm",
									},
									editor: datepicker2.open,
									editorParams:
									{
										optimusDatepicker:
										{
											type: 'time',
											allowKeyboardInput: store.touchscreen ? false : true,
											required: false
										}
									},
								},
								{
									field: 'description',
									title: 'Description',
									editor: 'input'
								},
							],
						data:
							[
								{ id: 1, date: '2024-04-06', datetime: '2024-04-06 10:31', time: '10:31', description: 'Première ligne' },
								{ id: 2, date: '2024-03-26', datetime: '2024-03-26 13:30', time: '13:30', description: 'Seconde ligne' },
								{ id: 3, date: null, datetime: null, time: null, description: 'Troisième ligne' }
							]
					}
				)

				loader(this.target, false)
			})
	}
}