export default class DevToolsClientComposantsTableDemo
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-devtools/client-doc/composants_table_demo.html', this.target)
		let config =
		{
			id: 'demotable',
			title: 'Dossiers',
			resource: 'dossiers',
			addIcon: 'fa-folder-plus',
			url: null,
			striped: true,
			bordered: true,
			fullscreen: true,
			tabulator:
			{
				progressiveLoad: false,
				columns:
					[
						{ field: "edit", title: '<i class="fas fa-pen is-clickable"></i>&nbsp;&nbsp;Editeur', titleFormatter: () => undefined, formatter: () => '<i class="fas fa-pen is-clickable"></i>', width: 41, resizable: false, headerSort: false, frozen: true, print: false, download: false, clipboard: false },
						{ field: "lastname", title: 'Nom' },
						{ field: 'firstname', title: 'Prénom' },
						{ field: 'email', title: 'Email' },
						{ field: 'phone', title: 'Téléphone' },
						{ field: 'mobile', title: 'Portable' },
					],
				ajaxURL: false,
				ajaxRequestFunc: false,
				ajaxConfig: {
					method: 'GET',
					credentials: 'include'
				},
				dataLoader: false,
				pagination: true,
				paginationSizeSelector: [25, 50, 100, 500, true],
				paginationCounter: "rows",
				ajaxContentType: 'json',
				movableColumns: true,
				layout: 'fitDataFill',
				layoutColumnsOnNewData: false,
				sortOrderReverse: true,
				headerSortElement: "<i class='fas fa-arrow-up'></i>",
				persistence: false,
				sortMode: 'local',
				filterMode: "local",
				paginationMode: 'local',
			},
		}

		let table = load('/components/optimus_table/index.js', this.target, config)
	}
}