export default class DevToolsClientBulmaPresentation
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/bulma_presentation.html', this.target)
			.then(() => loader(this.target, false))
	}
}