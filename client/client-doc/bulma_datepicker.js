export default class DevToolsClientBulmaDatepicker
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/bulma_datepicker.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				window.datepickerExample = datepicker('#datepicker-example')
				window.dateExample = datepicker('#date-example')
				window.timeExample = datepicker('#time-example')
				window.datetimeExample = datepicker('#datetime-example')
				window.daterangeExample = datepicker('#daterange-example')
				window.timerangeExample = datepicker('#timerange-example')
				window.datetimerangeExample = datepicker('#datetimerange-example')
				loader(this.target, false)
			})
	}
}