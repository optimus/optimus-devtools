export default class DevToolsClientBulmaModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/bulma_modal.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				loader(this.target, false)
			})
	}
}