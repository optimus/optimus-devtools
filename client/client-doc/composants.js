export default class DevToolsClientComposants
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/composants.html', this.target)
			.then(() => 
			{
				let tabs = [
					{
						id: "tab_composants_presentation",
						text: "Présentation",
						link: "/services/optimus-devtools/client-doc/composants_presentation.js",
						default: true,
						position: 100
					},
					{
						id: "tab_composants_table",
						text: "Table",
						link: "/services/optimus-devtools/client-doc/composants_table.js",
						position: 200
					},
					{
						id: "tab_composants_tabs",
						text: "Tabs",
						link: "/services/optimus-devtools/client-doc/composants_tabs.js",
						position: 300
					},
					{
						id: "tab_composants_calendar",
						text: "Calendar",
						link: "/services/optimus-devtools/client-doc/composants_calendar.js",
						position: 400
					},
					{
						id: "tab_composants_map",
						text: "Map",
						link: "/services/optimus-devtools/client-doc/composants_map.js",
						position: 500,
					},
					{
						id: "tab_composants_draglist",
						text: "DragList",
						link: "/services/optimus-devtools/client-doc/composants_draglist.js",
						position: 600
					},
					{
						id: "tab_composants_datepicker",
						text: "Datepicker",
						link: "/services/optimus-devtools/client-doc/composants_datepicker.js",
						position: 700
					},
					{
						id: "tab_composants_charts",
						text: "Charts",
						link: "/services/optimus-devtools/client-doc/composants_charts.js",
						position: 800
					},
					{
						id: "tab_composants_authorizations",
						text: "Authorizations",
						link: "/services/optimus-devtools/client-doc/composants_authorizations.js",
						position: 900
					},
					{
						id: "tab_composants_sortablelist",
						text: "Sortablelist",
						link: "/services/optimus-devtools/client-doc/composants_sortablelist.js",
						position: 1000
					},
					{
						id: "tab_composants_form",
						text: "Form",
						link: "/services/optimus-devtools/client-doc/composants_form.js",
						position: 1100
					}
				]

				load('/components/optimus_tabs.js', this.target,
					{
						router: true,
						tabs_container: document.getElementById('composants'),
						content_container: document.getElementById('composants_container'),
						tabs: tabs
					})
					.then(() => loader(this.target, false))
			})
	}
}