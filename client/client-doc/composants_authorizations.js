export default class DevToolsClientComposantsAuthorization
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/composants_authorizations.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				loader(this.target, false)
			})
	}
}