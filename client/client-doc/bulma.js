export default class DevToolsClientBulma
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/bulma.html', this.target)
			.then(() => 
			{
				let tabs = [
					{
						id: "tab_bulma_presentation",
						text: "Présentation",
						link: "/services/optimus-devtools/client-doc/bulma_presentation.js",
						default: true,
						position: 100
					},
					{
						id: "tab_bulma_icons",
						text: "Icons",
						link: "/services/optimus-devtools/client-doc/bulma_icons.js",
						position: 200
					},
					{
						id: "tab_bulma_datepicker",
						text: "Datepicker",
						link: "/services/optimus-devtools/client-doc/bulma_datepicker.js",
						position: 300
					},
					{
						id: "tab_bulma_slider",
						text: "Slider",
						link: "/services/optimus-devtools/client-doc/bulma_slider.js",
						position: 400
					},
					{
						id: "tab_bulma_switch",
						text: "Switch",
						link: "/services/optimus-devtools/client-doc/bulma_switch.js",
						position: 500
					},
					{
						id: "tab_bulma_tooltips",
						text: "Tooltips",
						link: "/services/optimus-devtools/client-doc/bulma_tooltips.js",
						position: 600,
					},
					{
						id: "tab_bulma_toast",
						text: "Toast",
						link: "/services/optimus-devtools/client-doc/bulma_toast.js",
						position: 700
					},
					{
						id: "tab_bulma_modal",
						text: "Modal",
						link: "/services/optimus-devtools/client-doc/bulma_modal.js",
						position: 800
					},
					{
						id: "tab_bulma_steps",
						text: "Steps",
						link: "/services/optimus-devtools/client-doc/bulma_steps.js",
						position: 900
					},
					{
						id: "tab_bulma_code",
						text: "Code",
						link: "/services/optimus-devtools/client-doc/bulma_code.js",
						position: 1000
					}
				]

				load('/components/optimus_tabs.js', this.target,
					{
						router: true,
						tabs_container: document.getElementById('bulma'),
						content_container: document.getElementById('bulma_container'),
						tabs: tabs
					})
					.then(() => loader(this.target, false))
			})
	}
}