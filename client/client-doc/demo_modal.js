export default class DevToolsClientDemoModal
{
	constructor(target, params) 
	{
		return load('/services/optimus-devtools/client-doc/demo_modal.html', target)
			.then(() => this.init())
	}

	init()
	{
		modal.querySelector('.button').onclick = () => modal.close()
		modal.querySelector('.modal-card-close').onclick = () => modal.close()
	}
}
