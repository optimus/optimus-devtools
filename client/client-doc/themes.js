export default class DevToolsClientThemes
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/themes.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				loader(this.target, false)
			})
	}
}