
export default class DevToolsClientComposantsChartsDemo
{
	constructor(target) 
	{
		this.target = target
	}

	async init()
	{
		await load('/services/optimus-devtools/client-doc/composants_charts_demo.html', this.target)

		let chart1 = await load('/components/optimus_chart.js', document.getElementById('democharts1'),
			{
				type: 'bar',
				data: {
					labels: ['Social', 'Immobilier', 'Pénal', 'Divorce', 'Recouvrements', 'Succession'],
					datasets: [{
						label: 'Dossiers ouverts',
						data: [12, 19, 8, 5, 2, 14],
						borderWidth: 1
					}]
				},
				options: {
					plugins: {
						legend: {
							display: false
						},
						title: {
							display: true,
							text: "REPARTITION DE L'ACTIVITE",
							font: {
								size: 20
							}
						}
					}
				}
			})

		//PIE
		let chart2 = await load('/components/optimus_chart.js', document.getElementById('democharts2'),
			{
				type: 'pie',
				data: {
					labels: [
						'Associé 1',
						'Associé 2',
						'Collaborateur'
					],
					datasets: [{
						label: 'CA Annuel',
						data: [300000, 250000, 100000],
						backgroundColor: [
							'rgb(255, 99, 132)',
							'rgb(54, 162, 235)',
							'rgb(255, 205, 86)'
						],
						hoverOffset: 4
					}]
				},
				options: {
					plugins: {
						title: {
							display: true,
							text: "REPARTITION DU CHIFFRE D'AFFAIRES",
						}
					}
				}
			})

		//LINE
		let chart3 = await load('/components/optimus_chart.js', document.getElementById('democharts3'),
			{
				type: 'line',
				data: {
					labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					datasets: [{
						data: [40000, 50000, 70000, 45000, 50000, 55000, 35000, 25000, 50000, 60000, 70000, 100000],
						fill: false,
						tension: 0.1
					}]
				},
				options: {
					plugins: {
						legend: {
							display: false
						},
						title: {
							display: true,
							text: "EVOLUTION DU CHIFFRE D'AFFAIRES"
						}
					}
				}
			})
	}
}