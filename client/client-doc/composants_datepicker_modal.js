load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
import { Tabulator, EditModule, FormatModule, InteractionModule, KeybindingsModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([EditModule, FormatModule, InteractionModule, KeybindingsModule])
load_script('/libs/luxon.min.js')

export default class DevToolsClientComposantsPresentation
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-devtools/client-doc/composants_datepicker_modal.html', this.target)
		modal.querySelector('.date-time-picker').onclick = event => load('/components/optimus_datepicker.js', modal, { input: event.target.parentNode.parentNode.querySelector('input') })
	}
}