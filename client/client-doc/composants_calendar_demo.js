export default class DevToolsClientComposantsCalendarDemo
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		let mycal = await load('/components/optimus_calendar/index.js', this.target)
			.then(component =>
			{
				let mycalendar = component.fullcalendar
				let event1_start = new Date(new Date().setDate(new Date().getDate() - 1))
				event1_start.setHours(9, 0, 0)
				let event1_end = new Date(new Date().setDate(new Date().getDate() - 1))
				event1_end.setHours(12, 0, 0)

				let event2_start = new Date(new Date().setDate(new Date().getDate() - 1))
				let event2_end = new Date(new Date().setDate(new Date().getDate() + 2))

				let event3_start = new Date(new Date().setDate(new Date().getDate() - 1))
				event3_start.setHours(11, 0, 0)
				let event3_end = new Date(new Date().setDate(new Date().getDate() - 1))
				event3_end.setHours(16, 0, 0)

				let event4_start = new Date(new Date().setDate(new Date().getDate() + 1))
				event4_start.setHours(14, 0, 0)
				let event4_end = new Date(new Date().setDate(new Date().getDate() + 1))
				event4_end.setHours(17, 0, 0)

				let event5_start = new Date()
				event5_start.setHours(12, 0, 0)
				let event5_end = new Date()
				event5_end.setHours(14, 0, 0)

				let event7_start = new Date(new Date().setDate(new Date().getDate() - 3))
				event7_start.setHours(10, 30, 0)
				let event7_end = new Date(new Date().setDate(new Date().getDate() - 3))
				event7_end.setHours(12, 30, 0)

				let event8_start = new Date(new Date().setDate(new Date().getDate() - 5))
				event8_start.setHours(8, 30, 0)
				let event8_end = new Date(new Date().setDate(new Date().getDate() - 5))
				event8_end.setHours(10, 30, 0)

				mycalendar.addEventSource([
					{
						id: 1,
						title: 'Evènement 1',
						start: event1_start,
						end: event1_end,
						color: '#ff6384'
					},
					{
						id: 2,
						allDay: true,
						title: 'Evènement 2',
						start: event2_start,
						end: event2_end,
						color: '#ffcd56'
					},
					{
						id: 3,
						title: 'Evènement 3',
						start: event3_start,
						end: event3_end,
						color: '#36a2eb',
						allDay: false
					},
					{
						id: 4,
						title: 'Evènement 4',
						start: event4_start,
						end: event4_end,
						color: '#36a2eb',
						allDay: false
					},
					{
						id: 5,
						title: 'Evènement 5',
						start: event5_start,
						end: event5_end,
						color: '#4bc0c0',
						allDay: false
					},
					{
						id: 6,
						title: 'Evènement 6',
						start: new Date(new Date().setDate(new Date().getDate() - 2)),
						color: '#36a2eb',
						allDay: true
					},
					{
						id: 7,
						title: 'Evènement 7',
						start: event7_start,
						end: event7_end,
						color: '#36a2eb',
						allDay: false
					},
					{
						id: 8,
						title: 'AGRILINE / JARDINERIE JAY (CA de Montpellier)',
						start: event8_start,
						end: event8_end,
						color: '#FBD75B',
						allDay: false
					},
					{
						id: 9,
						title: 'Evènement récurrent',
						rrule:
						{
							freq: 'daily',
							interval: 1,
							dtstart: event8_start,
							until: '2023-12-31'
						},
						duration: 7200000,
						allDay: false,
						color: '#800080',
					}
				])


				mycalendar.on('eventChange', function (changeInfo)
				{
					optimusToast("l'évènement " + changeInfo.event.title + ' a été modifié<br>Old Start : ' + changeInfo.oldEvent.startStr + '<br/>New Start : ' + changeInfo.event.startStr + '<br/>Old End : ' + changeInfo.oldEvent.endStr + '<br/>New End : ' + changeInfo.event.endStr)
				})

				mycalendar.on('dateClick', function (info)
				{
					optimusToast('dateClick detected<br/>Allday : ' + info.allDay + '<br/>Start : ' + info.dateStr)
				})

				mycalendar.on('eventClick', function (info) 
				{
					optimusToast('eventClick detected<br/>Allday : ' + info.event.allDay + '<br/>Start : ' + info.event.startStr + '<br/>End : ' + info.event.endStr)
				})

				mycalendar.on('select', function (info)
				{
					optimusToast('select detected<br/>Allday : ' + info.allDay + '<br/>Start : ' + info.startStr + '<br/>End : ' + info.endStr)
				})

				mycalendar.render()
				console.log(mycalendar)
			})
		console.log(mycal)
	}
}