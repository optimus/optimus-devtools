export default class DevToolsClientPresentation
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-devtools/client-doc/presentation.html', this.target)

		main.querySelector('.demo_tabs').onclick = () =>
		{
			let lis = main.querySelectorAll('.tabs > ul > li')
			for (let i = 1; i < lis.length; i++)
			{
				setTimeout(function () { lis[i].classList.remove('is-active') }, 100 * i + 100)
				setTimeout(function () { lis[i].classList.add('is-active') }, 100 * i)
			}
		}

		main.querySelector('.demo_modal_open').onclick = () => modal.open('/services/optimus-devtools/client-doc/demo_modal.js')
	}
}
