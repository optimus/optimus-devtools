export default class DevToolsClientComposantsCharts
{
	constructor(target) 
	{
		this.target = target
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/composants_charts.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				loader(this.target, false)
			})
	}
}