
document.getElementById('title').value = store.params.event.title
document.getElementById('start_date').value = store.params.event.startStr.slice(0, 10)
document.getElementById('start_time').value = store.params.event.startStr.slice(11, 16)
document.getElementById('end_date').value = store.params.event.startStr.slice(0, 10)
document.getElementById('end_time').value = store.params.event.startStr.slice(11, 16)
document.getElementById('allday').checked = store.params.event.allDay
document.getElementById('allday').onclick = function ()
{
	modal.querySelectorAll('.columns .column:nth-child(2)').forEach(column => column.classList.toggle('is-hidden'))
}

if (store.params.event.allDay == false)
	modal.querySelectorAll('.columns .column:nth-child(2)').forEach(column => column.classList.remove('is-hidden'))
else
	modal.querySelectorAll('.columns .column:nth-child(2)').forEach(column => column.classList.add('is-hidden'))

datepicker('#start_date')
datepicker('#start_time')
datepicker('#end_date')
datepicker('#end_time')

document.getElementById('cancel').onclick = modal.close

document.getElementById('delete').onclick = function ()
{
	window['democalendar'].getEventById(store.params.event.id).remove()
	modal.close()
}
