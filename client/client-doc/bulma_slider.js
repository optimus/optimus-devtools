export default class DevToolsClientBulmaSwitch
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/bulma_slider.html', this.target)
			.then(() => 
			{
				this.target.querySelector('.slider-with-tooltip').oninput = () =>
					this.target.querySelector('.slider-with-tooltip').dataset.tooltip = this.target.querySelector('.slider-with-tooltip').value

				highlight_code(main)
				loader(this.target, false)
			})
	}
}