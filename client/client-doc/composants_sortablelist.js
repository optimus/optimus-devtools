
export default class DevToolsClientComposantsDraglist
{
	constructor(target) 
	{
		this.target = target
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/composants_sortablelist.html', this.target)
			.then(() => 
			{
				highlight_code(main)

				import('/libs/sortable.min.js')
					.then(() =>
					{
						new Sortable(document.getElementById('draglist_demo'),
							{
								animation: 150,
							})

						new Sortable(document.getElementById('draglist_shared_1'),
							{
								group: 'shared',
								animation: 150
							})

						new Sortable(document.getElementById('draglist_shared_2'),
							{
								group: 'shared',
								animation: 150
							})

						new Sortable(document.getElementById('draglist_grid'),
							{
								animation: 150,
							})
					})

				loader(this.target, false)
			})
	}
}