export default class DevToolsClientServices
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/services.html', this.target)
			.then(() => 
			{
				highlight_code(main)
				loader(this.target, false)
			})
	}
}