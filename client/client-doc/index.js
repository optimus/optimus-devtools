export default class DevToolsClient
{
	constructor(target) 
	{
		this.target = target
	}

	async init()
	{
		await load('/services/optimus-devtools/client-doc/index.html', this.target)

		let tabs = [
			{
				id: "tab_presentation",
				text: "Présentation",
				link: "/services/optimus-devtools/client-doc/presentation.js",
				default: true,
				position: 100
			},
			{
				id: "tab_bulma",
				text: "Bulma",
				link: "/services/optimus-devtools/client-doc/bulma.js",
				position: 200
			},
			{
				id: "tab_leftmenu",
				text: "Left Menu",
				link: "/services/optimus-devtools/client-doc/leftmenu.js",
				position: 300
			},
			{
				id: "tab_topmenus",
				text: "Top Menus",
				link: "/services/optimus-devtools/client-doc/topmenus.js",
				position: 400
			},
			{
				id: "tab_notifications",
				text: "Notifications",
				link: "/services/optimus-devtools/client-doc/notifications.js",
				position: 500,
			},
			{
				id: "tab_progressbar",
				text: "ProgressBar",
				link: "/services/optimus-devtools/client-doc/progressbar.js",
				position: 600
			},
			{
				id: "tab_loader",
				text: "Loader",
				link: "/services/optimus-devtools/client-doc/loader.js",
				position: 700
			},
			{
				id: "tab_composants",
				text: "Composants",
				link: "/services/optimus-devtools/client-doc/composants.js",
				position: 800
			},
			{
				id: "tab_router",
				text: "Router",
				link: "/services/optimus-devtools/client-doc/router.js",
				position: 500
			},
			{
				id: "tab_services",
				text: "Services",
				link: "/services/optimus-devtools/client-doc/services.js",
				position: 900
			},
			{
				id: "tab_pwa",
				text: "PWA",
				link: "/services/optimus-devtools/client-doc/pwa.js",
				position: 1000
			},
			{
				id: "tab_themes",
				text: "Themes",
				link: "/services/optimus-devtools/client-doc/themes.js",
				position: 1100
			},
			{
				id: "tab_rest",
				text: "Client REST",
				link: "/services/optimus-devtools/client-doc/rest.js",
				position: 1200
			},
			{
				id: "tab_websocket",
				text: "Websocket",
				link: "/services/optimus-devtools/client-doc/websocket.js",
				position: 1300
			}
		]

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('client_doc'),
				content_container: document.getElementById('client_doc_container'),
				tabs: tabs
			})
	}
}