
export default class DevToolsClientComposantsDraglist
{
	constructor(target) 
	{
		this.target = target
	}

	init()
	{
		loader(this.target)
		load('/services/optimus-devtools/client-doc/composants_draglist.html', this.target)
			.then(() => 
			{
				highlight_code(main)

				load('/components/optimus_draglist/index.js', main.querySelector('#draglist_demo'),
					{
						senderTitle: 'Services disponibles',
						senderData:
							[
								{ id: 'optimus-cloud', displayname: "Cloud" },
								{ id: 'optimus-avocats', displayname: "Avocats" },
								{ id: 'optimus-calendar', displayname: "Agenda" },
								{ id: 'optimus-structures', displayname: "Cabinet" },
								{ id: 'barolibre', displayname: "BaroLibre" },
								{ id: 'optimus-devtools', displayname: "Outils de développement" },
							],
						senderDrop: row => console.log('ADD', row.data),

						receiverTitle: 'Services installés',
						receiverData: [],
						receiverDrop: row => console.log('REMOVE', row.data)
					})

				loader(this.target, false)
			})
	}
}