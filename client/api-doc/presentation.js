export default class DevToolsApiPresentation
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/presentation.html', this.target)
			.then(() => highlight_code(main))
	}
}
