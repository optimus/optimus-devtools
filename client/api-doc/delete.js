export default class DevToolsApiDelete
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/delete.html', this.target)
			.then(() => highlight_code(main))
	}
}
