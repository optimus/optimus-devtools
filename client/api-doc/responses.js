export default class DevToolsApiResponses
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/responses.html', this.target)
			.then(() => highlight_code(main))
	}
}
