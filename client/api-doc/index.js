export default class DevToolsApi
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-devtools/api-doc/index.html', this.target)

		let tabs =
			[
				{
					id: "tab_presentation",
					text: "Présentation",
					link: "/services/optimus-devtools/api-doc/presentation.js",
					default: true,
					position: 100
				},
				{
					id: "tab_requests",
					text: "Requêtes",
					link: "/services/optimus-devtools/api-doc/requests.js",
					position: 200
				},
				{
					id: "tab_responses",
					text: "Réponses",
					link: "/services/optimus-devtools/api-doc/responses.js",
					position: 300
				},
				{
					id: "tab_resources",
					text: "Ressources",
					link: "/services/optimus-devtools/api-doc/resources.js",
					position: 400
				},
				{
					id: "tab_authorizations",
					text: "Autorisations",
					link: "/services/optimus-devtools/api-doc/authorizations.js",
					position: 500,
				},
				{
					id: "tab_get",
					text: "GET",
					link: "/services/optimus-devtools/api-doc/get.js",
					position: 600
				},
				{
					id: "tab_post",
					text: "POST",
					link: "/services/optimus-devtools/api-doc/post.js",
					position: 700
				},
				{
					id: "tab_patch",
					text: "PATCH",
					link: "/services/optimus-devtools/api-doc/patch.js",
					position: 800
				},
				{
					id: "tab_delete",
					text: "DELETE",
					link: "/services/optimus-devtools/api-doc/delete.js",
					position: 900
				}
			]

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('api_doc'),
				content_container: document.getElementById('api_doc_container'),
				tabs: tabs
			})
	}
}
