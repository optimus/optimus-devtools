export default class DevToolsApiRequests
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/requests.html', this.target)
			.then(() => highlight_code(main))
	}
}
