export default class DevToolsApiGet
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/get.html', this.target)
			.then(() => highlight_code(main))
	}
}
