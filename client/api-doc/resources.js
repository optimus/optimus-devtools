export default class DevToolsApiResources
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/resources.html', this.target)
			.then(() => highlight_code(main))
	}
}
