export default class DevToolsApiPatch
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/patch.html', this.target)
			.then(() => highlight_code(main))
	}
}
