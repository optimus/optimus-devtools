export default class DevToolsApiAuthorizations
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/authorizations.html', this.target)
	}
}
