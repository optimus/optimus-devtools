export default class DevToolsPost
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/services/optimus-devtools/api-doc/post.html', this.target)
			.then(() => highlight_code(main))
	}
}
