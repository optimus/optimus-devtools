export default class OptimusDevtools
{
	constructor()
	{
		this.name = 'optimus-devtools'
		this.swagger_modules =
			[
				{
					id: "optimus-devtools-docker",
					title: "OPTIMUS DEVTOOLS (DOCKER)",
					path: "/services/optimus-devtools/optimus-docker.yaml",
					filters: ["Container", "Image", "Network", "Volumes", "Exec", "Swarm", "Nodes", "Services", "Service", "Task", "Secret", "Config"]
				},
				{
					id: "optimus-devtools-websocket",
					title: "OPTIMUS DEVTOOLS (WEBSOCKET)",
					path: "/services/optimus-devtools/optimus-websocket.yaml",
					filters: ["broadcast"]
				},
			]
		store.services.push(this)
	}

	login()
	{
		topmenus['helpmenu'].add("helpmenu-devtools", "header", "Pour les développeurs")
		topmenus['helpmenu'].add("helpmenu-devtools-wiki", "item", "Wiki", "fa-brands fa-wikipedia-w", () => window.open('https://wiki.cybertron.fr', '_blank'))
		topmenus['helpmenu'].add("helpmenu-devtools-git", "item", "Git", "fa-brands fa-gitlab", () => window.open('https://git.cybertron.fr', '_blank'))

		topmenus.create('optimus-devtools', 'fas fa-code')
		topmenus['optimus-devtools'].add("doc-client", "item", "Documentation (client web)", "fas fa-book", () => router('optimus-devtools/client-doc#presentation'))
		topmenus['optimus-devtools'].add("doc-api", "item", "Documentation (api)", "fas fa-book", () => router('optimus-devtools/api-doc#presentation'))
		topmenus['optimus-devtools'].add('store', 'item', 'Afficher le store', 'fas fa-store', () => load('/services/optimus-devtools/store/index.js', main))
		topmenus['optimus-devtools'].add('swagger', 'item', 'Swagger', 'fas fa-code', () => router('optimus-devtools/swagger-menu'))
		topmenus['optimus-devtools'].add('fontawesome', 'item', 'Font Awesome', 'fa-brands fa-font-awesome', () => window.open('https://fontawesome.com/search?o=r&m=free'))
		topmenus['optimus-devtools'].add('gitlab', 'item', 'GitLab', 'fa-brands fa-gitlab', () => window.open('https://git.cybertron.fr/optimus'))
		topmenus['optimus-devtools'].add('delete_user_preferences', 'item', 'Supprimer Préférences', 'fas fa-trash', () => delete_user_preferences())

		if (store.user.admin == 1)
			rest(store.user.server + '/optimus-base/server/preferences/branch', 'POST', 'dev')

		document.querySelector('.optimus-websocket-indicator').classList.remove('is-hidden')
		if (optimusWebsocket.readyState == 1)
			document.querySelector('.optimus-websocket-indicator').classList.remove('is-invisible')

		optimusWebsocket.addEventListener('open', () =>
		{
			document.querySelector('.optimus-websocket-indicator').classList.remove('is-invisible')
			optimusWebsocket.addEventListener('close', () => document.querySelector('.optimus-websocket-indicator').classList.add('has-text-danger'))
			optimusWebsocket.addEventListener('message', () =>
			{
				document.querySelector('.optimus-websocket-indicator').classList.add('has-text-success')
				setTimeout(() => document.querySelector('.optimus-websocket-indicator').classList.remove('has-text-success'), 75)
			})
		})
	}

	logout() 
	{
		topmenus['optimus-devtools'].remove()

		topmenus['helpmenu'].querySelector('#helpmenu-devtools').remove()
		topmenus['helpmenu'].querySelector('#helpmenu-devtools-wiki').remove()
		topmenus['helpmenu'].querySelector('#helpmenu-devtools-git').remove()

		if (store.user.admin == 1)
			rest(store.user.server + '/optimus-base/server/preferences/branch', 'POST', 'beta')

		document.querySelector('.optimus-websocket-indicator').classList.add('is-hidden')
		document.querySelector('.optimus-websocket-indicator').classList.add('is-invisible')
		optimusWebsocket.removeEventListener('open', this.websocketOpenEventListener)
		optimusWebsocket.removeEventListener('close', this.websocketCloseEventListener)
		optimusWebsocket.removeEventListener('message', this.websocketMessageEventListener)

		store.services = store.services.filter(service => service.name != this.id)
	}

	global_search() { }

	dashboard() { }
}