export default class DevToolsSwaggerMenu
{
	constructor(target, params) 
	{
		return load('/services/optimus-devtools/swagger-menu/index.html', target)
			.then(() => this.init())
	}

	init()
	{
		let swagger_modules = new Array
		for (let service of store.services)
			if (service.swagger_modules)
				swagger_modules = swagger_modules.concat(service.swagger_modules)
		swagger_modules.sort((a, b) => (a.id < b.id) ? -1 : ((b.id > a.id) ? 1 : 0))

		for (let swagger_module of swagger_modules)
		{
			let template = document.querySelector('#swagger_module_template').content.cloneNode(true)

			let title_link = document.createElement('a')
			title_link.innerText = swagger_module.title
			title_link.href = "javascript:router('optimus-devtools/swagger?module=" + swagger_module.path + "')"
			template.querySelector('.title').appendChild(title_link)

			if (swagger_module.filters)
				for (let filter of swagger_module.filters)
				{
					let filter_link = document.createElement('a')
					filter_link.innerText = filter.toLowerCase()
					filter_link.href = "javascript:router('optimus-devtools/swagger?module=" + swagger_module.path + "&filter=" + filter + "')"
					template.querySelector('.filters').appendChild(filter_link)

					if (filter != swagger_module.filters[swagger_module.filters.length - 1])
					{
						let separator = document.createElement('span')
						separator.innerText = ' - '
						template.querySelector('.filters').appendChild(separator)
					}
				}

			document.querySelector('#swagger_modules').appendChild(template)
		}
	}
}