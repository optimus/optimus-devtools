await load_script('/libs/swagger-ui/swagger-ui-bundle.js')
await load_CSS('/libs/swagger-ui/swagger-ui.css')
export default class DevToolsSwagger
{
	constructor(target, params) 
	{
		return load('/services/optimus-devtools/swagger/index.html', target)
			.then(() => this.init())
	}

	init()
	{
		const ui = SwaggerUIBundle(
			{
				url: store.queryParams.module,
				dom_id: '#swagger-ui',
				withCredentials: store.queryParams.module == 'jurisprudence' ? false : true,
				persistAuthorization: true,
				docExpansion: 'list',
				filter: store.queryParams.filter || true,
				jsonEditor: false,
				tryItOutEnabled: true,
				plugins: [
					SwaggerUIBundle.plugins.DownloadUrl,
					function ()
					{
						return {
							statePlugins: {
								spec: {
									wrapActions: {
										updateJsonSpec: function (oriAction, system)
										{
											return (spec) =>
											{
												if (store.queryParams.module.includes('/optimus-'))
												{
													if (spec.swagger == '2.0')
														spec.host = spec.host.replace("{apiserver}", store.user.server)
													else
													{

													if (!spec.servers)
														spec.servers = Array()
														spec.servers[0].variables = Object()
														spec.servers[0].variables.apiserver = Object()
														spec.servers[0].variables.apiserver.enum = Array()
														spec.servers[0].variables.apiserver.default = store.user.server
														spec.servers[0].variables.apiserver.enum.push(store.user.server)
														spec.servers[0].variables.apiserver.enum.push('api.demoptimus.fr')
														spec.servers[0].variables.apiserver.enum.push('api.devoptimus.ovh')
													}
												}
												
												return oriAction(spec)
											}
										}
									}
								}
							}
						}
					}
				],
				onComplete: function () 
				{
					document.querySelectorAll('.version').forEach(element => element.style.background = 'transparent')

					if (store.queryParams.module == 'jurisprudence')
						ui.preauthorizeApiKey("admin", "b5099546-de05-4943-8abd-ad6672cbe841")
				}
			})
		window.ui = ui
	}
}

