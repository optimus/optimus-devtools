<?php
use optimus\OptimusWebsocket;

$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "notifications.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"colorclass": { "type": "module", "field": "notifications.colorclass", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "is-info" },
	"icon": { "type": "string", "field": "notifications.icon", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "fas fa-bell" },
	"title": { "type": "string", "field": "notifications.title", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "Nouvelle notification" },
	"text": { "type": "text", "field": "notifications.text", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"date": { "type": "datetime", "field": "notifications.date", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "' . date('Y-m-d H:i:s') . '"},
	"link": { "type": "string", "field": "notifications.link", "post": ["undefinedtonull", "emptytonull"], "patch": ["notnull", "notempty"]},
	"archived": { "type": "boolean", "field": "notifications.archived", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 }
}
', null, 512, JSON_THROW_ON_ERROR);

$post = function ()
{
	global $resource, $input;
	auth();
	admin_only();
	allowed_origins_only();

	if ($_GET['recipients'] != "all" AND $_GET['recipients'] != "admin" AND $_GET['recipients'] != "user")
		return array("code" => 400, "message" => "recipients ne peut avoir que l'une des valeurs suivantes : all, admin ou user");

	if ($_GET['recipients'] == "user" AND (!is_numeric($_GET['user_id']) OR ($_GET['user_id'] <= 0)))
		return array("code" => 400, "message" => "user_id doit être un nombre entier strictement positif");
	else if ($_GET['recipients'] == "user")
		$_GET['recipients'] = $_GET['user_id'];

	check_input_body($resource, 'post');

	$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
	$websocket->send(json_encode(array("command" => "notification", "user" => $_GET['recipients'], "data" => $input->body)));
	return array("code" => 201, "data" => $input->body);
};
?>