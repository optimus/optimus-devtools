<?php
include('libs/docker_socket.php');
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();
	$endpoint = check('endpoint', str_replace('/optimus-devtools/docker','',$input->url['path']), 'string', true);
	$response = docker_socket_request("GET /v1.41" . $endpoint . ($_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : ''));
	return array("code" => $response['code'], "header" => $response['header'], "body" => $response['body']);
};


$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();
	$endpoint = check('endpoint', str_replace('/optimus-devtools/docker','',$input->url['path']), 'string', true);
	$response = docker_socket_request("POST /v1.41" . $endpoint . ($_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : ''));
	return array("code" => $response['code'], "header" => $response['header'], "body" => $response['body']);
};


$patch = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();
	$endpoint = check('endpoint', str_replace('/optimus-devtools/docker','',$input->url['path']), 'string', true);
	$response = docker_socket_request("PATCH /v1.41" . $endpoint . ($_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : ''));
	return array("code" => $response['code'], "header" => $response['header'], "body" => $response['body']);
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();
	$endpoint = check('endpoint', str_replace('/optimus-devtools/docker','',$input->url['path']), 'string', true);
	$response = docker_socket_request("DELETE /v1.41" . $endpoint . ($_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : ''));
	return array("code" => $response['code'], "header" => $response['header'], "body" => $response['body']);
};


$head = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();
	$endpoint = check('endpoint', str_replace('/optimus-devtools/docker','',$input->url['path']), 'string', true);
	$response = docker_socket_request("HEAD /v1.41" . $endpoint . ($_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : ''));
	return array("code" => $response['code'], "header" => $response['header'], "body" => $response['body']);
};
?>